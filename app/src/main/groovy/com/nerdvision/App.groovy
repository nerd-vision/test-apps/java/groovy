/*
 * This Groovy source file was generated by the Gradle 'init' task.
 */
package com.nerdvision

import com.nerdvision.api.NerdVisionAPI

class App {
    static void main(String[] args) {
        final SimpleTest ts = new SimpleTest("This is a test");
        for (; ;) {
            try {
                ts.message ts.newId();
            }
            catch (Exception e) {
                NerdVisionAPI.captureException e;
                e.printStackTrace();
                ts.reset();
            }

            Thread.sleep 100;
        }
    }
}
