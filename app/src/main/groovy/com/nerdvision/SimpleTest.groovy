/**
 * Copyright (C) 2019 Intergral Information Solutions GmbH. All Rights Reserved
 */

package com.nerdvision;

/**
 *
 * @version 1.0.0* @author nwightma
 */
class SimpleTest extends BaseTest {
    public static Date NICE_START_DATE = new Date();

    private final long startedAt = System.currentTimeMillis();
    private final String testName;
    private int maxExecutions;
    private int cnt = 0;
    private Map<Character, Integer> charCounter = new TreeMap<Character, Integer>();


    SimpleTest(final String testName) {
        this.testName = testName;
        this.maxExecutions = nextMax();
    }

    void message(final String uuid) throws Exception {
        System.out.println(cnt + ":" + uuid);
        cnt += 1;

        checkEnd(cnt, maxExecutions);

        final Map<Character, Integer> info = makeCharCountMap(uuid);
        merge(charCounter, info);
        if ((cnt % 30) == 0) {
            dump();
        }
    }


    void merge(final Map<Character, Integer> charCounter, final Map<Character, Integer> newInfo) {
        for (final Character c : newInfo.keySet()) {
            final Integer i = newInfo.get(c);

            Integer curr = charCounter.get(c);
            if (curr == null) {
                charCounter.put(c, i);
            } else {
                charCounter.put(c, curr + i);
            }
        }
    }


    void dump() {
        System.out.println(charCounter);
        charCounter = new HashMap<Character, Integer>();
    }


    void checkEnd(final int val, final int max) throws Exception {
        if (val > max) {
            throw new Exception("Hit max executions " + val + " " + max);
        }
    }


    @Override
    String toString() {
        return getClass().getName() + ":" + testName + ":" + startedAt + "#" + System.identityHashCode(this);
    }


    void reset() {
        this.cnt = 0;
        this.maxExecutions = this.nextMax();
    }
}
