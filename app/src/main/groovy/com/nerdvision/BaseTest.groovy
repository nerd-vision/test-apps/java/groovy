package com.nerdvision;

import java.util.concurrent.ThreadLocalRandom;

class BaseTest {
    protected final Properties systemProps = System.getProperties();


    String newId() {
        UUID.randomUUID().toString();
    }


    protected int nextMax() {
        return ThreadLocalRandom.current().nextInt(1, 100);
    }


    Map<Character, Integer> makeCharCountMap(final String str) {
        final HashMap<Character, Integer> res = new HashMap<Character, Integer>();

        for (int i = 0; i < str.length(); i++) {
            final char c = str.charAt(i);
            final Integer cnt = res.get(c);
            if (cnt == null) {
                res.put(c, 0);
            } else {
                res.put(c, cnt + 1);
            }
        }

        return res;
    }
}
